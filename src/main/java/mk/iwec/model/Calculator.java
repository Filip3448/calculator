package mk.iwec.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Calculator {
	private double firstNumber;
	private double secondNumber;
	private String operator;

	public double calculateResult() {
		double result = 0;
		switch (this.operator) {
		case "+":
			result = this.firstNumber + this.secondNumber;
			break;
		case "-":
			result = this.firstNumber - this.secondNumber;
			break;
		case "*":
			result = this.firstNumber * this.secondNumber;
			break;
		case "/":
			result = this.firstNumber / this.secondNumber;
			break;
		case "^":
			result = Math.pow(this.firstNumber, this.secondNumber);
			break;
		}
		return result;
	}
}
