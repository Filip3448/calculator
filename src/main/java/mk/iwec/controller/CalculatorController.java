package mk.iwec.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import mk.iwec.model.Calculator;

@Controller
public class CalculatorController {

	@GetMapping("/")
	public String index(Model model) {
		model.addAttribute("operator", "+");
		model.addAttribute("view", "views/calculatorForm");
		return "base-layout";
	}

	@PostMapping("/")
	public String index(@RequestParam String firstNumber, @RequestParam String operator,
			@RequestParam String secondNumber, Model model) {
		double leftNumber;
		double rightNumber;

		try {
			leftNumber = Double.parseDouble(firstNumber);

		} catch (NumberFormatException ex) {
			leftNumber = 0;
		}
		try {
			rightNumber = Double.parseDouble(secondNumber);
		} catch (NumberFormatException ex) {
			rightNumber = 0;
		}
		Calculator calculator = new Calculator(leftNumber, rightNumber, operator);
		double result = calculator.calculateResult();

		model.addAttribute("firstNumber", leftNumber);
		model.addAttribute("operator", operator);
		model.addAttribute("secondNumber", rightNumber);
		model.addAttribute("result", result);

		model.addAttribute("view", "views/calculatorForm");
		return "base-layout";
	}

}
